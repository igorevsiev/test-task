import { boot } from 'quasar/wrappers'
import { reactive } from 'vue'
import model from '../store/index'

export default boot(({ app }) => {
    app.config.globalProperties.$store = reactive(model)
  })