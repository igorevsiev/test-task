import axios from 'axios'
import { Loading, Notify, LocalStorage } from 'quasar'
import urls from '../api/url'
const querystring = require('querystring')

let getterObjs = function () {
    return this.state
}

let getterObjCount = function () {
    return this.countObj
}

let loadObjList = function (args) {
    let url = urls.GET_LIST + querystring.stringify(args)
    Loading.show()
    axios.get(url).then((response) => {
        if (response.data.status == "ok") {
            this.state = response.data.message.tasks
            this.countObj = response.data.message.total_task_count
            Loading.hide()
        } else {
            console.log(response.data.message)
            Notify.create({
                color: 'red',
                message: Object.values(response.data.message).join()
            })
        }
    })
}

let actionObj = function (url, args) {
    Loading.show()
    return axios.post(url, querystring.stringify(args)).then((response) => {
        Loading.hide()
        if (response.data.status == "ok") {
            if (url.includes('create')) {
                this.state.unshift(response.data.message)
            } else {
                this.state.map((obj) => {
                    if (obj.id === args.id) {
                        obj.text = args.text
                        obj.status = args.status
                    }
                })
            }
            Notify.create({
                color: 'green',
                message: "Success"
            })
        } else {
            console.log(response.data.message)
            Notify.create({
                color: 'red',
                message: Object.values(response.data.message).join()
            })
        }
    })
}

let createObj = function (args) {
    return actionObj.call(this, urls.CREATE_TASK, args)
}

let editObj = function (id, args) {
    return actionObj.call(this, `${urls.EDIT_TASK}/${id}?developer=iev&`, args)
}

export default {
  state: [],
  countObj: 0,
  login: false,
  getterObjs: getterObjs,
  getterObjCount: getterObjCount,
  loadObjList: loadObjList,
  createObj: createObj,
  editObj: editObj
}

