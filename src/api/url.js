
const BASE_URL = 'https://uxcandy.com/~shapoval/test-task-backend/v2'
const suffix = `?developer=iev&`

export default {
    GET_LIST: `${BASE_URL}/${suffix}`,
    CREATE_TASK: `${BASE_URL}/create${suffix}`,
    EDIT_TASK: `${BASE_URL}/edit`,
    LOGIN_URL: `${BASE_URL}/login${suffix}`
}
